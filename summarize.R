library(dplyr)

## Apply some stat processing to the data :
## Gives count, mean, median, min, max, standard deviation, standard error of the mean, and confidence interval (default 95%).
summarySE <- function(data, conf.interval = .95) {
  data_stats <- data %>%
    filter(!is.na(value)) %>%
    group_by(x, variable) %>%
    summarise(N = sum(!is.na(value)), # sum(!is.na(value)) counts values without NA
              sd = sd(value, na.rm = TRUE),
              min = min(value, na.rm = TRUE),
              max = max(value, na.rm = TRUE),
              median = median(value, na.rm = TRUE),
              mean = mean(value, na.rm = TRUE))

  data_stats$se <- data_stats$sd / sqrt(data_stats$N) # Compute standard error of the mean

  # Confidence interval multiplier for standard error
  # Calculate t-statistic for confidence interval: 
  # e.g., if conf.interval is .95, use .975 (above/below), and use df=N-1
  ciMult <- qt(conf.interval / 2 + .5, data_stats$N - 1)
  data_stats$ci <- data_stats$se * ciMult

  return(data_stats)
}

## Melt + merge function used when comparing two dataframes:
## Both dataframes are melted to isolate the provided variable(s), and then the two results are merged
melt_merge <- function(data, compare_data, data_var, compare_data_var, id.vars = "x") {
  melted_data <- melt(data = data, id.vars = id.vars, measure.vars = data_var)
  melted_compare_data <- melt(data = compare_data, id.vars = id.vars, measure.vars = compare_data_var)

  return(merge(melted_data, melted_compare_data, all = TRUE, sort = FALSE))
}

## Melt + merge function used when comparing three dataframes:
## All dataframes are melted to isolate the provided variable(s), and then the results are merged
melt_merge_3 <- function(data1, data2, compare_data, data_var1, data_var2, compare_data_var, id.vars = "x") {
  melted_data1 <- melt(data = data1, id.vars = id.vars, measure.vars = data_var1)
  melted_data2 <- melt(data = data2, id.vars = id.vars, measure.vars = data_var2)
  melted_compare_data <- melt(data = compare_data, id.vars = id.vars, measure.vars = compare_data_var)

  return(merge(merge(melted_data1, melted_data2, all = TRUE, sort = FALSE), melted_compare_data, all = TRUE, sort = FALSE))
}

## Melt + merge function used when comparing four dataframes:
## All dataframes are melted to isolate the provided variable(s), and then the results are merged
melt_merge_4 <- function(data1, data2, data3, compare_data, data_var1, data_var2, data_var3, compare_data_var, id.vars = "x") {
  melted_data1 <- melt(data = data1, id.vars = id.vars, measure.vars = data_var1)
  melted_data2 <- melt(data = data2, id.vars = id.vars, measure.vars = data_var2)
  melted_data3 <- melt(data = data3, id.vars = id.vars, measure.vars = data_var3)
  melted_compare_data <- melt(data = compare_data, id.vars = id.vars, measure.vars = compare_data_var)

  return(
    merge(
      merge(
        merge(melted_data1, 
              melted_data2, all = TRUE, sort = FALSE), 
        melted_data3, all = TRUE, sort = FALSE), 
      melted_compare_data, all = TRUE, sort = FALSE))
}

## Melt + merge function used when comparing four dataframes:
## All dataframes are melted to isolate the provided variable(s), and then the results are merged
melt_merge_5 <- function(data1, data2, data3, data4, compare_data, data_var1, data_var2, data_var3, data_var4, compare_data_var, id.vars = "x") {
  melted_data1 <- melt(data = data1, id.vars = id.vars, measure.vars = data_var1)
  melted_data2 <- melt(data = data2, id.vars = id.vars, measure.vars = data_var2)
  melted_data3 <- melt(data = data3, id.vars = id.vars, measure.vars = data_var3)
  melted_data4 <- melt(data = data4, id.vars = id.vars, measure.vars = data_var4)
  melted_compare_data <- melt(data = compare_data, id.vars = id.vars, measure.vars = compare_data_var)

  return(
    merge(
      merge(
        merge(
          merge(melted_data1, 
                melted_data2, all = TRUE, sort = FALSE), 
          melted_data3, all = TRUE, sort = FALSE), 
        melted_data4, all = TRUE, sort = FALSE),
      melted_compare_data, all = TRUE, sort = FALSE)
  )
}